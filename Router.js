import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Login from './Pages/login'
import Profile from './Pages/profile'
import EditProfile from './Pages/editProfile'
import List from './Pages/list'
import Product from './Pages/product'
import EditProduct from './Pages/editProduct'
import AddProduct from './Pages/addProduct'

class Router extends Component {

    render() {
        return(
            <NativeRouter>
                <Switch>
                    <Route exact path = '/login' component = {Login}/>
                    <Route exact path = '/profile' component = {Profile}/>
                    <Route exact path = '/list' component = {List}/>
                    <Route exact path = '/product' component = {Product}/>
                    <Route exact path = '/addProduct' component = {AddProduct}/>
                    <Route exact path = '/editProfile' component = {EditProfile}/>
                    {/*
                    <Route exact path = 'editProduct' component = {EditProduct}/>
                     */}
                    <Redirect to = '/login'/>
                </Switch>
            </NativeRouter>
        )
    }
}

export default Router