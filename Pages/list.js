import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import { Link } from 'react-router-native'


class List extends Component {

    onPressAdd = () => {
        this.props.history.push('/addProduct')
    }

    onPressMyProfile = () => {
        this.props.history.push('/profile')
    }

    onPressImage = () => {
        this.props.history.push('/product')
    }


    render() {
        return (
            // //TODO: เปลี่ยน <Text> ให้เป็น <Image>
            <ScrollView>
                <View style={[styles.header, styles.center]}>
                    <Text style={{ fontSize: 30 }}>Product List</Text>
                </View>
                <View style={[styles.container, styles.column]} >
                    <View style={[styles.row, styles.center]}>
                        <TouchableOpacity onPress={this.onPressImage}>
                            <Text style={styles.box}>image1</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.box}>image1</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.row, styles.center]}>
                        <Text style={styles.box}>image1</Text>
                        <Text style={styles.box}>image1</Text>
                    </View>
                    <View style={[styles.row, styles.center]}>
                        <Text style={styles.box}>image1</Text>
                        <Text style={styles.box}>image1</Text>
                    </View>
                    <View style={[styles.row, styles.center]}>
                        <Text style={styles.box}>image1</Text>
                        <Text style={styles.box}>image1</Text>
                    </View>
                    <View style={[styles.row, styles.center]}>
                        <Text style={styles.box}>image1</Text>
                        <Text style={styles.box}>image1</Text>
                    </View>
                    <View style={[styles.row, styles.center]}>
                        <Text style={styles.box}>image1</Text>
                        <Text style={styles.box}>image1</Text>
                    </View>
                    <View style={[styles.row, styles.center]}>
                        <Text style={styles.box}>image1</Text>
                        <Text style={styles.box}>image1</Text>
                    </View>
                </View>
                <View style={[styles.footer, styles.row, styles.center]}>
                    <TouchableOpacity onPress={this.onPressAdd}>
                        <Text style={{ fontSize: 30, margin: 14 }}>add</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.onPressMyProfile}>
                        <Text style={{ fontSize: 30, margin: 14 }}>my profile</Text>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
        flex: 1,
        backgroundColor: 'red',
        height: 70
    },
    container: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    column: {
        flex: 1,
        flexDirection: 'column'
    },
    box: {
        width: 100,
        height: 100,
        backgroundColor: 'red',
        flex: 1,
        margin: 14
    },
    footer: {
        flex: 1,
        backgroundColor: 'green',
        height: 60
    }

});

export default List