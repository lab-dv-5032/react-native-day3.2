import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Image, TextInput, TouchableOpacity, TouchableHighlight } from 'react-native'
import { Link } from 'react-router-native'

class Profile extends Component {

    onPressBack = () => {
        this.props.history.push('/list')
    }

    onPressEditProfile = () => {
        this.props.history.push('/editProfile')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.header, styles.row]}>
                    <TouchableOpacity onPress={this.onPressBack} style={{alignItems: 'flex-start'}}>
                        <Text style={{ fontSize: 30, margin: 14 }}>Back</Text>
                    </TouchableOpacity>
                    <Text style={{ fontSize: 30, margin: 14, alignItems: 'flex-end' }}>My Profile</Text>
                </View>
                <View style={[styles.content, styles.column]}>
                    <Text>Username</Text>
                    <Text style={{ fontSize: 30 }}>Username</Text>
                    <Text>First name</Text>
                    <Text style={{ fontSize: 30 }}>First name</Text>
                    <Text>Last name</Text>
                    <Text style={{ fontSize: 30 }}>Last name</Text>
                </View>
                <View style={[styles.footer, styles.center]}>
                    <TouchableOpacity style={styles.touchable}>
                        <Text>Edit profile</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
        flex: 1,
        backgroundColor: 'red',
        height: 60
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    column: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        flex: 1
    },
    footer: {
        flex: 1
    },
    touchable: {
        alignItems: 'center',
        padding: 20,
        backgroundColor: 'darkgrey',
        margin: 14,
    }
})

export default Profile