import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'


class Login extends Component {

  state = {
    username: '',
    password: ''
  }

  onPressLogin = () => {
    this.props.history.push('/list', {username: this.state.username, password:this.state.password})
  }

  render() {
    return (
      <View style={[styles.container]}>
      <View style={[styles.content, styles.center]}>
        <View style={[styles.image, styles.center]}>
          <Image style={styles.image} source={{ uri: 'https://66.media.tumblr.com/2598fd4b8401d84ff4497ad91abf1ab1/tumblr_p24gdnvMLh1sowt91o8_500.png' }} />
        </View>
      </View>
      


      <View style={styles.content}>
        <Text style={{ alignItems: 'center' }}>{this.state.username} - {this.state.password}</Text>
        <View>
          <TextInput onChangeText={(value) => { this.setState({ username: value }) }} value={this.state.username} style={styles.textInput} placeholder='Username' />
        </View>

        <View>
          <TextInput onChangeText={(value) => { this.setState({ password: value }) }} value={this.state.password} style={styles.textInput} placeholder='Password' />
        </View>


        <TouchableOpacity onPress={this.onPressLogin}>
          <View style={styles.touchable}>
            <Text>Login</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2'
  },
  image: {
    alignItems: 'center',
    backgroundColor: 'white',
    width: 200,
    height: 200,
    borderRadius: 100

  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  textInput: {
    margin: 8,
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50
  },
  touchable: {
    alignItems: 'center',
    padding: 20,
    backgroundColor: 'darkgrey',
    margin: 14,

  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  row: {
    flex:1,
    flexDirection: 'row'
  }


});


export default Login